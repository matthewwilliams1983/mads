from enum import Enum

class SoundTypes(Enum):
    TALKING = 0
    SILIENCE = 1
    GENERICDRONE = 2