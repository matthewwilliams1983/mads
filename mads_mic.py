from audiolazy import sHz, chunks, AudioIO, line
import collections
import threading

class Microphone:

    ########################################################################################
    # This class is the microphone and the thread that the microphone runs in.
    #
    # @instance var self.rate - The sampling rate. Default of 44100.
    #
    # @instance var self.Hz - The normalized frequency, 2 pi / rate. Used in MADSRFFT to compute frequency bins.
    #                         Ex: rate 44100 - 2 pi / rate = 0.00014247585730565955
    #                         See: https://en.wikipedia.org/wiki/Normalized_frequency_(unit)
    #                         See: https://i.stack.imgur.com/cJtxS.png
    #
    # @instance var self.s - Need other eyes on this, not used.
    #
    # @instance var self.api - This establishes where the line in is coming from. Default is None which uses
    #                          the default line. If an arg is supplied, AudioIO will search through the list of
    #                          available APIs and raiser a runtime error if the supplied API is not found.
    #
    # @instance var self.chunks - NEED other eyes on this not sure we need this.
    #
    # @instance var length - The size of the buffer for the microphone data (the deque).
    #
    # @instance var data - The deque that holds the time domain values of the microphone.
    #
    # @instance var finish - The boolean if the thread for updating the microphone data should continue
    #                        running.
    #
    # @instance microphone - This is the microphone object that contains the microphone data
    #                        needed for the rfft.
    ########################################################################################

    def __init__(self, rate=40000, api=None, chunks=16, length=2 ** 12):
        self.rate = rate
        self.s, self.Hz = sHz(rate)
        self.api = api
        self.chunks = chunks
        self.length = length
        self.data = collections.deque([0.] * length, maxlen=self.length)
        self.finish = False

# _______________________________________________________________________________________________________END of __init__

    def get_mic_data(self):
        with AudioIO(api=self.api) as rec:
            for el in rec.record(rate=self.rate):
                self.data.append(el)
                if self.finish:
                    break
# __________________________________________________________________________________________END of function get_mic_data

    def start_thread(self):
        mic_thread = threading.Thread(target=self.get_mic_data)
        mic_thread.finish = False
        mic_thread.start()  # Already start updating data

# __________________________________________________________________________________________END of function start_thread

# _______________________________________________________________________________________________END of class Microphone
