# README #

Below is the details associated with this project.

### Description ###

This is the project I created for team projects II. The purpose of this project is to use audio signals to detect drones through machine learning

Version 1.0

### How do I get set up? ###

* Dependencies: You will need Python 3 installed as well as the following: audiolazy, numpy, scipy, pyaudio, and scikit-learn

* Configuration: In the mads.py file there are 7 global variables. The first two (BIN_LOWER, BIN_UPPER) define frequency ranges to consider when making a signature.
The next two (BIN_SIZE, NUMBER_OF_PEAK_FREQUENCIES) define the how wide a range a frequency will be considered unique (was done to avoid spectral leakage) the other global defines how many spectral peaks are considered for a signature
The last three globals (DETECTION_MODE, SIGNATURE_GENERATION_MODE, CLEAR_TRAINING_DATABASE) are booleans that switch the operation of the system from training the database or detecting drones.
WARNING: If you set the CLEAR_TRAINING_DATABASE to True, you will need to retrain the data with your own values.
Make sure you have an active microphonoe attached to your computing device before running the application or the backend process will crash.

* Database configuration: The database associated with this project is a simple csv file where the first column indicates the classification (see mads_sound_types.py) and the remaining columns indicate the spectral peaks.
This database is used by the machine learning algorithm to try to predict if the incoming audio belongs to whatever you have set for it look for (currently setup for UASs)

### Special Notes ###

This project needs a few things done to it in its current state(always): better data preprocessing, further the training data, saving the AI model after all the training is more robust, optimization, implementation of stft over rfft for possibly doing instantaneous detections

### Who do I talk to? ###

Repo owner or admin: Matthew Williams matthewwilliams83@hotmail.com