from enum import Enum

class Alarm_State(Enum):
    WAITING = 1
    ENABLED = 2
    DISABLED = 3