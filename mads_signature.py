# Imports from MADS
from mads_sound_types import SoundTypes

# External imports
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler

class MADSSignature:

    def __init__(self, detection_mode=True, signature_generation_mode=False, clear_training_database=True):

        self.detection_mode = detection_mode
        self.signature_generation_training_mode = signature_generation_mode
        self.clear_training_database = clear_training_database
        self.detection_count = 0
        self.counter = 0
        self.if_not_in_finger_list = []
        self.training_file = "mads_training_data.csv"
        self.drone_type = SoundTypes.WEEDWACKER
        self.chain_five_fft = 0

        # Model Classifier
        self.drone_classifier = None
        self.scalar = StandardScaler()

        if self.clear_training_database:
            open(self.training_file, 'w').close()

# _______________________________________________________________________________________________________END of __init__

    def load_detection_model(self):

        drone_data = np.loadtxt('mads_training_data.csv', delimiter=',')

        # Setting up the data into attributes(X) classifiers (y)
        X = drone_data[:, 1:]
        y = drone_data[:, 0]

        self.scalar.fit(X)
        X = self.scalar.transform(X)
        self.drone_classifier = MLPClassifier(solver='lbfgs', alpha=1e-5)
        self.drone_classifier.fit(X, y)

# ______________________________________________________________________________END of function set_drone_signature_data

    def check_current_signature_against_model(self, fingerprint_finder, total_energy):

        current_fft = list(np.sort(fingerprint_finder.binned_indices))

        if total_energy >= .001:

            X_new = [current_fft]
            X_new = np.asarray(X_new)
            X_new = self.scalar.transform(X_new)

            predict = self.drone_classifier.predict(X_new)
            print(predict)
            if predict <= 50:
                self.detection_count += 1
                print("COUNT:", self.detection_count, predict)
                if self.detection_count >= 1000:
                    return True, current_fft
                else:
                    return False, False
            else:
                self.detection_count = 0
                return False, False
        else:
            self.detection_count = 0
            return False, False

# _______________________________________________________________________________________END of function check_signature

    def generate_signatures_for_training_data(self, fingerprint_finder, total_energy):

        final = list(np.sort(fingerprint_finder.binned_indices))

        if total_energy >= .001:
            final.insert(0, self.drone_type.value)
            if final not in self.if_not_in_finger_list:
                self.if_not_in_finger_list.append(final)
                csv_string_of_signature = ','.join(map(str, final))
                with open(self.training_file, 'a') as f:
                    f.write(csv_string_of_signature + '\n')
# ___________________________________________________________________________________END of function generate_signatures

# ____________________________________________________________________________________________END of class MADSSignature
